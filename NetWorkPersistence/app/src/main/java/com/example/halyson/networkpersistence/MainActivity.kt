package com.example.halyson.networkpersistence

import android.arch.persistence.room.Room
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import model.God
import picasso.PicassoUtil
import retrofit.RetrofitUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import room.DatabaseConnect

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


            val isConnectedWifi = ConnectionUtil.validConnection(this)
            if (isConnectedWifi) {
                recuperarDeus()
            } else {
                var database = DatabaseConnect.getSingleton(this)
                if (database != null) {
                    val godDao = database.godDao()
                    val god = godDao.getAll()
                    if (god != null) {
                        montarDeus(god.get(0))
                        Toast.makeText(this@MainActivity, "Consulta em banco Deus:" + god?.get(0)?.deus, Toast.LENGTH_LONG).show()
                    }
                }
            }



    }

    fun recuperarDeus() {
        val callBack = object : Callback<God> {
            override fun onFailure(call: Call<God>?, t: Throwable?) {
                Toast.makeText(this@MainActivity, "Erro ao capturar o Json", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<God>?, response: Response<God>?) {
                val god = response?.body()
                if (god != null) {
                    montarDeus(god)
                    insertRoom(god)
                }
            }

        }
        RetrofitUtil.getSmite(callBack)

    }

    fun montarDeus(god: God) {
        txtGod.text = god.deus
        txtMitologia.text = god.mitologia
        txtTipo.text = god.tipo
        txtDescricac.text = god.descricao
        PicassoUtil.downloadImagem(god.img,this,imageView)
    }



    fun insertRoom(god: God) {
        var database = DatabaseConnect.getSingleton(this)

        val godDao = database.godDao()
        godDao.insert(God(1, god.deus, god.mitologia, god.tipo, god.descricao,god.img))

    }


}
