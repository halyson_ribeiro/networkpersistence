package com.example.halyson.networkpersistence

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.widget.Toast

class ConnectionUtil {

    companion object {
        fun validConnection(ctx: Context): Boolean {
            val connectWifi = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networks = connectWifi.allNetworks
            for (n in networks) {
                val info = connectWifi.getNetworkInfo(n)
                if (info.state == NetworkInfo.State.CONNECTED) {
                    Toast.makeText(ctx, "Connected Wifi", Toast.LENGTH_LONG).show()
                    return true;
                } else {
                    Toast.makeText(ctx, "Not Connected Wifi", Toast.LENGTH_LONG).show()
                    return false;
                }
            }
            return false;
        }
    }
}