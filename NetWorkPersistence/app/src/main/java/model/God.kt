package model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class God (@PrimaryKey(autoGenerate = true)var id: Long,var deus : String, var mitologia : String, var tipo :String, var descricao :String, var img: String) {

}