package picasso

import android.content.Context
import android.widget.ImageView
import com.squareup.picasso.Picasso

class PicassoUtil {

    companion object {
        fun downloadImagem(urlImg: String, ctx: Context, imageView: ImageView) {

            Picasso.with(ctx).load(urlImg).into(imageView)

        }
    }

}