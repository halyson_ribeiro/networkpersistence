package retrofit

import model.God
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitUtil {

    companion object{

        val retrofit = Retrofit.Builder().baseUrl("https://api.myjson.com/").addConverterFactory(GsonConverterFactory.create()).build()
       val smiteInterface = retrofit.create(SmiteI::class.java)

        fun getSmite(cb : Callback<God>){
            smiteInterface.getGod().enqueue(cb)
        }

    }





}