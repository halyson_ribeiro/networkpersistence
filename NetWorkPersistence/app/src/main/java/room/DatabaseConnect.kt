package room

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import model.God


@Database(entities = arrayOf(God::class), version = 1, exportSchema = false)
abstract class DatabaseConnect : RoomDatabase() {
    abstract fun godDao(): GodDao


    companion object {
        var database: DatabaseConnect? = null

        fun getSingleton(ctx: Context): DatabaseConnect {
            if (database == null) {
                database = Room.databaseBuilder(ctx, DatabaseConnect::class.java, "smite").allowMainThreadQueries().build()
            }
            return database as DatabaseConnect
        }
    }

}