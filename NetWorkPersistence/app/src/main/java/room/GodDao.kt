package room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import model.God

@Dao
interface GodDao {
    @Query("SELECT * FROM God")
    fun getAll(): List<God>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg god: God)
}